/*
   4-probe WiFI Meat Thermometer: WMT4
   
   
   ESP8266 prototype 

   Copyright 2016 Rob Redford
   This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
   To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.

   Portions of the thermistor code are Copyright 2013 Tony DiCola (tony@tonydicola.com).
   Released under an MIT license: http://opensource.org/licenses/MIT

   References:
   Uses Blynk library for smart phone interface: http://www.blynk.cc/
        Docs: 							http://docs.blynk.cc/
   Adafruit WiFi thermometer reference: https://learn.adafruit.com/cloud-thermometer/hardware (software)
   Hardware references: 				https://www.reddit.com/r/arduino/comments/2kmgvg/wifi_meat_thermometer_with_trinket_esp8266/
   										http://imgur.com/aP3nQIK (reference schematic)
   										https://github.com/CapnBry/HeaterMeter/wiki/HeaterMeter-Probes
	Temperature probe (Maverick ET-73): http://www.amazon.com/dp/B004W8B3PC/ref=sr_ph?ie=UTF8&qid=1456787599&sr=1&keywords=maverick+et-73
        									Note the nominal resistance of this probe at 25 deg C is 200k. 
   											This must also be the value of the series resistor in the voltage divider circuit.
    LCD screen:							http://www.sainsmart.com/sainsmart-iic-i2c-twi-serial-2004-20x4-lcd-module-shield-for-arduino-uno-mega-r3.html
   											Note: I2C address on the web page is incorrect
	LCD character creation tool:		http://www.quinapalus.com/hd44780udg.html
	LCD library:						https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
											Note: see the library manual pages (in the source) for special char creation & use
	Theory reference:					https://en.wikipedia.org/wiki/Steinhart-Hart
	
	For ESP8266 pin definitions, see: AppData\Local\Arduino15\packages\esp8266\hardware\esp8266\2.1.0\variants\adafruit\pins_arduino.h
*/
 


/* ================================= physical LCD =================================================================================================== */

#include <Wire.h>
#include <LiquidCrystal_I2C.h>


/*
   pins on the I2C backpack on the LCD
   these pins are not exposed to the Arduino but are necessary to define for the library
   (see class instantiation below)
   pin definitions don't match schematics - extracted from http://arduino.arigato.cz/I2C_LCD_BackPack_for_1602_and_2004/LCD_16x2_BackPack_v003.ino
*/
#define BACKLIGHT_pin	3
#define EN_pin  			2
#define RW_pin  			1
#define RS_pin  			0
#define D4_pin  			4
#define D5_pin  			5
#define D6_pin  			6
#define D7_pin  			7

/*
   LCD defines
*/
#define LCD_ADDR				0x27			// SainSmart 2004. See I2C scanner utility in Test folder
#define LCD_ROWS				4 
#define LCD_COLS				20

#define	OFFLINE_COUNT		3				// when to insert a message about being offline in LCD update


// create LCD screen instance - note that pin numbers differ from library defaults so have to set them explicity in the constructor here

LiquidCrystal_I2C	display(LCD_ADDR, EN_pin, RW_pin, RS_pin, D4_pin, D5_pin, D6_pin, D7_pin);

/* ================================= Blynk =================================================================================================== */

//#define BLYNK_DEBUG	
//#define BLYNK_PRINT 		Serial    			// Comment this out to disable prints and save space - always declare BEFORE Blynk header files
//#define DebugSerial		Serial
//#define DEBUG										// enable BLYNK_PRINT and a corresponding serial output to capture the stream

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <SimpleTimer.h>							// multiple, polled timer events without using a hardware (AVR) timer, but (much) less precision wrt the interval


/*
   probe and related thermistor defines
   Probe 1 must be connected to A0 and then sequentially from there
*/
#define PROBE1_TEMP				V0				// THIS GROUP MUST BE SEQUENTIAL
#define PROBE2_TEMP				V1
#define PROBE3_TEMP				V2
#define PROBE4_TEMP				V3

#define PROBE1_SETPT				V4				// THIS GROUP MUST BE SEQUENTIAL
#define PROBE2_SETPT				V5
#define PROBE3_SETPT				V6
#define PROBE4_SETPT				V7

#define	PROBE1_LED				V8
#define	PROBE2_LED				V9
#define	PROBE3_LED				V10
#define	PROBE4_LED				V11

#define	CONNECTED_LED			V12

#define	P1_CONNECTED			V13
#define	P2_CONNECTED			V14
#define	P3_CONNECTED			V15
#define	P4_CONNECTED			V16

#define	CLONE_P1					V17

/*
   Blynk objects
*/
WidgetLED led_done_1(PROBE1_LED);
WidgetLED led_done_2(PROBE2_LED);
WidgetLED led_done_3(PROBE3_LED);
WidgetLED led_done_4(PROBE4_LED);
WidgetLED led_ok(CONNECTED_LED);
WidgetLED led_present_1(P1_CONNECTED);
WidgetLED led_present_2(P2_CONNECTED);
WidgetLED led_present_3(P3_CONNECTED);
WidgetLED led_present_4(P4_CONNECTED);


char 	auth[] = "<<INSERT AUTH STRING HERE>>";	// Blynk interface with the history graph

/* ================================= SPI A to D converter MPC3204 =================================================================================================== */

#include "MCP320x.h"

#define	ADC_ZERO_THRESHOLD		10				// threshold value for a zero reading (capacitance?)
#define ADC_SAMPLES        		5				// number of sames to taken when reading analog ports for temp values

MCP320x MCP(SS);								// see pins_arduino.h for ESP8266/Huzzah - using only 1 parameter forces SPI MODE (uses SPI library)

/* ================================= sketch =================================================================================================== */

/*
   Steinhart-Hart coefficients obtained empirically from the calibration utility for the Maverik ET-73 200k ohm probe
   See https://en.wikipedia.org/wiki/Steinhart-Hart for reference and readTemp() below
*/
#define	A						0.001433954477
#define B						0.000076877069
#define C						0.000000537597


/*
   other defines
*/
#define	PROBE_COUNT			4				// number of temperature probes 
#define	ADC_RESOLUTION		4095			// ADC resolution: depends on the platform. Mega/Uno is 10 bits, so (2^10)-1 = 1023. This is for the 12-bit MPC3204 SPI ADC



/*
   data associated with each temperature probe
*/
typedef struct {
	uint16_t		Setpoint;					// setpoint
	uint16_t		SavedSetpoint;				// copy to save for a disconnected probe
	uint16_t		CurrentTemp;				// current temperature. 
	uint8_t		SetpointReached :1;		// if set temperature has been reached
	uint8_t		Present         :1;		// true if probe is connected
	uint8_t		TempChanged     :1;		// true if temp has changed - throttles updates to Blynk dashboard
	uint8_t		BlynkOffline    :1;		// set after a reconnect event so updateSetpoint can sync the slider values
} ProbeType;

ProbeType Probe[PROBE_COUNT];
uint32_t seriesResistor[PROBE_COUNT] = { 197900, 200500, 197800, 198800 };		// Measured series resistor value in ohms. Must be the SAME as the nominal 25deg C resistance of the probe

//create arrays of LED widgets to make indexing the LEDs easier
WidgetLED *LED_done[PROBE_COUNT];
WidgetLED *LED_present[PROBE_COUNT];

/* ================================================== Timer functions =========================================================================== */

// stagger the intervals to allow time for the server messages to complete. Also, use primes in case that helps.
#define	SETPT_DELAY			1609			// how often setpoints are checked in msec
#define PROBE_DELAY			1009			// how often probe temperatures are read in msec
#define TEMP_DELAY			1289			// how often dashboard temperature display values are pushed

#define LCD_DELAY				3049			// how often physical LCD is updated (slow enough to minimize flicker)
#define PUSH_DELAY			30000			// how often push notifications for setpoint reached are done in msec


SimpleTimer timer;

/* ==========================================================  GENERAL ================================================================================ */

bool	redrawScreen = false;							// set when a screen redraw is needed
bool	firstConnect = true;							// true after we have connected once

/* ================================= CODE =================================================================================================== */

void displaySetup(void);

/*
 display static elements of physical LCD display
*/
void displaySetup ( void ) {
	char buffer[18];

	for ( int i = 0; i < PROBE_COUNT; i++ ) {
		sprintf_P(buffer, PSTR("P%d Tmp:    Set:"), i+1);
		display.setCursor(0, i);
		display.print(buffer);
	}
}

/*
 SETUP
*/
void setup ( void ) {
	uint8_t customChar[2][8] = {
								{ 0x1f, 0x11, 0x11, 0x11, 0x11, 0x11, 0x1f },		// open square, position 0
								{ 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f }		// filled square, position 1
								};

	
#if defined(DEBUG)
	DebugSerial.begin(9600);
	while ( !DebugSerial );
#endif // DEBUG
	Blynk.begin(auth, "<<SSID>>", "<<PASSWORD>><");
	
	/*
	 LED widget array initialization
	 This simplifies widget referencing in loops
	 Note: we can't declare an array of widgets from the outset becuase there is no default constructor for the class
	*/
	LED_done[0] = &led_done_1;
	LED_done[1] = &led_done_2;
	LED_done[2] = &led_done_3;
	LED_done[3] = &led_done_4;
	
	LED_present[0] = &led_present_1;
	LED_present[1] = &led_present_2;
	LED_present[2] = &led_present_3;
	LED_present[3] = &led_present_4;


	// LCD initialization
	display.begin(LCD_COLS, LCD_ROWS);
	display.setBacklightPin(BACKLIGHT_pin, POSITIVE);
	display.setBacklight(HIGH);

	display.createChar(0, (uint8_t *)customChar[0]);		// special characters for setpoint status
	display.createChar(1, (uint8_t *)customChar[1]);
	
	display.clear();
	display.setCursor(0, 1);								// col, row
	display.print(F("  Start Blynk app"));
	display.setCursor(0, 2);
	display.print(F(" BEFORE controller"));
	delay(3000);
	
	display.clear();
	display.setCursor(0, 0);
	display.print(F("WMT4 V2.1.0"));
	display.setCursor(0, 1);
	display.print(F(__DATE__));
	display.setCursor(12, 1);
	display.print(F(__TIME__));
	display.setCursor(0, 3);
	display.print(F("Connecting ..."));
	delay(2000);
	
	MCP.setMCPConfig(MCP_SINGLE, MCP_ALL_PORTS);			// single mode for all MPC3204 pins


	while ( !Blynk.connect() ) {							// wait for connection
		Blynk.run();
	}				

	display.clear();
	display.setCursor(5, 1);
	display.print(F("Connected"));


	//  initialization for Blynk widget values
	for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
		Probe[i].Setpoint = 0;
		Probe[i].SavedSetpoint = 0;
		Probe[i].CurrentTemp = 0;
		Probe[i].SetpointReached = false;
		Probe[i].TempChanged = true;
		Probe[i].BlynkOffline = false;
		/*
		   Probes are connected with a 3-pin 2.5mm jack. The jack shorts the sense pin (same side as the shield pin) to the center connector (on the opposite side).
		   Thus, if we ground the sense pin the port pin is shorted to ground and reading this analog port value will return 0 indicating the probe is NOT present.
		*/
		if ( MCP.readChannel(i) == 0 ) {
			Probe[i].Present = false;
		} else {
			Probe[i].Present = true;
		}
	}
	
	/*
	   initialize the Blynk dashboard (app) status displays in case they are still set from last session
	   Note that you CAN write to a slider virtual pin from the sketch as well as setting it in the app
	   Delays below are needed to prevent choking Blynk. 
	*/
	for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
		LED_done[i]->off();
		if ( Probe[0].Present ) {
			LED_present[i]->on();
		} else {
			LED_present[i]->off();
		}
		//delay(100);						// throttle initialization?
	}
	delay(1000);

	// we rely on sequential vpin numbering to make all of the loops in this sketch work
	for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
		Blynk.virtualWrite((PROBE1_SETPT+i), 0);
	}
	delay(1000);
  
	/*
		Program flow:
		
		Event										Handler				Dashboard updated
		-----------------------------   -------------------   ----------------
		Setpoint set in dashboard			BLYNK_WRITE()			YES
		Probe temperature change			updateProbes()
		Probe removed							updateProbes()			(YES)
		Probe re-inserted						updateProbes()			(YES)
		Dashboard Temp display update		updateTemps()			YES
		Setpoint reached						updateSetpoints()
		Dashboard setpt LED updated		updateSetpoints()		YES
		Setpoint push notices				sendPushNotices()
		LCD updated								updateLCD()
	*/

	timer.setInterval(SETPT_DELAY, updateSetpoints);
	timer.setInterval(PROBE_DELAY, updateProbes);
	timer.setInterval(TEMP_DELAY, updateTemps);
	timer.setInterval(PUSH_DELAY, sendPushNotices);
	timer.setInterval(LCD_DELAY, updateLCD);
	
	display.clear();
	displaySetup();
}

/*
   BLYNK_WRITE called when an input widget is writing to the device/sketch -  e.g. a value is being set
   In the cases below, the dashboard is updating the probe setpoint temperatures
   
   If we need to *force* a new value, we have to push it to the app since it will never poll for an update (e.g. BLYNK_READ())
   
   A note on Blynk conventions: a function in all caps means that the context is the dashbaord(smart phone app). e.g.
		BLYNK_WRITE: dashboard ==> sketch
   In mixed case the sketch is the context. e.g.
		Blynk.virtualWrite: sketch ==> dashboard
  
   If the probe is not present, force the setpoint back to 0
   We update the LED status here as well in case the dashboard app started after the sketch and thus did not get initialized in setup()
   This does not create too much unnecessary traffic as these functions are only called when the setup is changed in the app (infrequent)
   and it is our first indication that the app is online and being used.
   
   General note on LEDs: always check current state before changing to limit number of server requests
*/
BLYNK_WRITE ( PROBE1_SETPT ) {
	if ( Probe[0].Present ) { 
		Probe[0].Setpoint = param.asInt();
		Probe[0].SavedSetpoint = Probe[0].Setpoint;
		led_present_1.on();
		if ( Probe[0].BlynkOffline ) {
			// clear the flag now that the value has been updated
			Probe[0].BlynkOffline = false;
		}
	} else {
		Blynk.virtualWrite(PROBE1_SETPT, 0);
		// reduce server req by checking the value of the LED, which is done locally
		if ( led_present_1.getValue() > 0 ) {
			led_present_1.off();
		}
	}
}

BLYNK_WRITE ( PROBE2_SETPT ) {
	if ( Probe[1].Present ) {
		Probe[1].Setpoint = param.asInt();
		Probe[1].SavedSetpoint = Probe[1].Setpoint;
		led_present_2.on();
		if ( Probe[1].BlynkOffline ) {
			Probe[1].BlynkOffline = false;
		}
	} else {
		Blynk.virtualWrite(PROBE2_SETPT, 0);
		if ( led_present_2.getValue() > 0 ) {
			led_present_2.off();
		}
	}
}

BLYNK_WRITE ( PROBE3_SETPT ) {
	if ( Probe[2].Present ) {
		Probe[2].Setpoint = param.asInt();
		Probe[2].SavedSetpoint = Probe[2].Setpoint;
		led_present_3.on();
		if ( Probe[2].BlynkOffline ) {
			Probe[2].BlynkOffline = false;
		}
	} else {
		Blynk.virtualWrite(PROBE3_SETPT, 0);
		if ( led_present_3.getValue() > 0 ) {
			led_present_3.off();
		}
	}
}

BLYNK_WRITE ( PROBE4_SETPT ) {
	if ( Probe[3].Present ) {
		Probe[3].Setpoint = param.asInt();
		Probe[3].SavedSetpoint = Probe[3].Setpoint;
		led_present_4.on();
		if ( Probe[3].BlynkOffline ) {
			Probe[3].BlynkOffline = false;
		}
	} else {
		Blynk.virtualWrite(PROBE4_SETPT, 0);
		if ( led_present_4.getValue() > 0 ) {
			led_present_4.off();
		}
	}
}

/*
   this push button will take the setpoint for P1 and copy it to all the other probes
*/
BLYNK_WRITE ( CLONE_P1 ) {
	if ( Probe[0].Present && (Probe[0].Setpoint > 0) ) {
		for ( uint8_t i = 1; i < PROBE_COUNT; i++ ) {
			if ( Probe[i].Present ) {
				Probe[i].Setpoint = Probe[0].Setpoint;
				Probe[i].SavedSetpoint = Probe[0].Setpoint;
				// need to push the new value so the dashboard gets it
				Blynk.virtualWrite((PROBE1_SETPT+i), Probe[0].Setpoint);
			}
		}
	}
}


/*
  Called when connected to the server
  This will also get called if we are disconnected for some reason then reconnected by Blynk.run()
*/
BLYNK_CONNECTED () {
	if ( firstConnect ) {
		firstConnect = false;
		Blynk.notify("Connected");
		led_ok.on();
	} else {
		Blynk.notify("Reconnected");
		// set flag to have updateSetpoints capture any updates to setpoints while we were offline
		for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
			if ( Probe[i].Present ) {
				Probe[i].BlynkOffline = true;
			}
		}
		redrawScreen = true;
	}
}

/*
 Check current values to see if probe setpoints have been reached
 Note that for the setpoint flag to be set, it must be present. Thus, we don't need to check this in other functions
 Also updates Blynk dashboard setpoint LEDs
*/
void updateSetpoints ( void ) {
	for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
		if ( Probe[i].Present ) {
			if ( (Probe[i].Setpoint > 0) && (Probe[i].CurrentTemp >= Probe[i].Setpoint) ) {
				Probe[i].SetpointReached = true;
			} else {
				Probe[i].SetpointReached = false;
			}
			if ( Blynk.connected() ) {
				if ( Probe[i].BlynkOffline ) {
					/*
					  resync slider value - may have changed while we were offline
					  we now need to wait until the next update cycle to see if the setpoint was reached
					  the Blynk sync request will generate a BLYNK_WRITE. To make sure this happens (e.g. did not
					  go offline again in the interim) we clear the offline flag only when we get the new value
					*/
					Blynk.syncVirtual((PROBE1_SETPT+i));
				} else {
					if ( Probe[i].SetpointReached ) {
						if ( LED_done[i]->getValue() == 0 ) {
							LED_done[i]->on();
						}
					} else {
						// turn off in case temp has dropped below threshold after going over
						if ( LED_done[i]->getValue() > 0 ) {
							LED_done[i]->off();
						}
					}
				}
			}
		}
	}
}

/*
  Update dashboard temp displays
  Temp displays are set to "push" instead of a regular update interval to reduce update frequency and constipating the system
*/
void updateTemps ( void ) {
		if ( Blynk.connected() ) {
			for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
				if ( Probe[i].Present && Probe[i].TempChanged ) {
					Probe[i].TempChanged = false;
					Blynk.virtualWrite((PROBE1_TEMP+i), Probe[i].CurrentTemp);
				}
			}
		}

}

/*
  Send notifications for probes that have reached their setpoint values
*/
void sendPushNotices ( void ) {
	String	message = "Done:";
	bool	sendNotice = false;

	if ( Blynk.connected() ) {
		for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
			// setpoint flag can only be set if the probe is marked as Present
			if ( Probe[i].SetpointReached ) {
				message += " ";
				message += String(i+1);
				sendNotice = true;
			}
		}
		if ( sendNotice ) {
			Blynk.notify(message);
		}
	}
}


/* 
   Derive the resistance of the probe to use in the Steinhart-Hart eqution.
   We can do this given the known value of the series resistor in the fixed part of the voltage divider
   and the fact that since V = I*R the voltage drop on each resistor is directly proportional to voltage.
   Thus, we can use a ratio to determine the resistance of the probe without using the reference voltage which can vary considerably from 5V.
   Also, this allows us to use the same code on 5V or 3.3V platforms :-) as well as just working with the digitized reading without any voltage conversions.
   
   NOTE: both float and double on Arduino are 32-bit values. No difference between the two.
*/
float calcResistance ( const uint8_t pin ) {
	float reading = 0;

	for ( uint8_t i = 0; i < ADC_SAMPLES; i++ ) {
		reading += MCP.readChannel(pin);
	}
	if ( reading <= (ADC_ZERO_THRESHOLD * ADC_SAMPLES) ) {
		// probe disconnected
		return 0;
	} else {
		reading /= (float)ADC_SAMPLES;
		reading = (ADC_RESOLUTION / reading) - 1;
		return (seriesResistor[pin] / reading);
	}
}

/*
  get the normalized raw value of the probe on the given pin for debug
  note that a disconnected probe will return 0 
*/
uint16_t probeRaw ( const uint8_t pin ) {
	uint16_t reading = 0;

	for ( uint8_t i = 0; i < ADC_SAMPLES; i++ ) {
		reading += MCP.readChannel(pin);
	}
	reading /= ADC_SAMPLES;
	return reading;
}

/*
   read the temperature from the given analog pin and return temperature in degrees F
   uses the Steinhart-Hart equation
   Note: when using constants in float calculations, make sure there is a decimal point
*/
float readTemp ( const uint8_t pin ) {
	float R, kelvin;

	R = calcResistance(pin);
	if ( R == 0 ) {
		// probe disconnected
		return 0;
	} else {
		kelvin = 1.0/(A + B*log(R) + C*pow(log(R), 3.0));
		return ((kelvin  * (9.0/5.0)) - 459.67);				// K to F conversion
	}
}

/*
  Get current probe temps
  Check for probes that have been disconnected or reconnected
     - update Blynk dashboard if probe status has changed
*/
void updateProbes ( void ) {
	for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
		uint16_t temp = (uint16_t)readTemp(i);
		
		// get probe temps & handle disconnects/reconnects
		if ( temp != Probe[i].CurrentTemp ) {
			Probe[i].CurrentTemp = temp;
			Probe[i].TempChanged = true;
		}
		if ( (Probe[i].CurrentTemp == 0) && Probe[i].Present ) {
			/*
			   probe disconnected:
			   push dashboard setpoint slider value
			   temperature will be updated with the next BLYNK_READ of the probe temp
			*/
			Probe[i].Present = false;
			Probe[i].Setpoint = 0;
			Probe[i].SetpointReached = false;
			redrawScreen = true;
			if ( Blynk.connected() ) {
				if ( LED_present[i]->getValue() > 0 ) {
					LED_present[i]->off();
				}
				if ( LED_done[i]->getValue() > 0 ) {
					LED_done[i]->off();
				}
				Blynk.virtualWrite((PROBE1_SETPT+i), 0);
			}
		} else  if ( (Probe[i].CurrentTemp > 0) && !Probe[i].Present ) {
			// reinserted - reset to previous setpoint
			Probe[i].Present = true;
			Probe[i].Setpoint = Probe[i].SavedSetpoint;
			redrawScreen = true;
			if ( Blynk.connected() ) {
				// push restored values to the app - these are not polled
				if ( LED_present[i]->getValue() == 0 ) {
					LED_present[i]->on();
				}
				Blynk.virtualWrite((PROBE1_SETPT+i), Probe[i].Setpoint);
			}
		}
	}
}

/*
 Update the physical LCD display
 This happens even when Blynk is offline
 Periodically display an offline message when Blynk is offline
*/

void updateLCD ( void ) {
	static uint8_t	offlineCount = 0;
	
	if ( !Blynk.connected() ) {
		++offlineCount;
	}
	if ( offlineCount >= OFFLINE_COUNT ) {
		// Blynk.run() will reconnect if needed, but this can happen when there is a long delay before reconnecting
		display.clear();
		display.setCursor(4, 1);
		display.print(F("No connection"));
		
		offlineCount = 0;
		redrawScreen = true;
	} else {
		if ( redrawScreen ) {
			display.clear();
			displaySetup();
			redrawScreen = false;
		}
		for ( uint8_t i = 0; i < PROBE_COUNT; i++ ) {
			if ( Probe[i].Present ) {
				char	buffer[21];
				
#if defined(DEBUG)
				int raw = probeRaw(i);
				DebugSerial.print(i); DebugSerial.print(": "); DebugSerial.print(raw); DebugSerial.print(" T: "); DebugSerial.print(Probe[i].CurrentTemp);
				DebugSerial.print(" V: "); DebugSerial.println(MCP.rawToVoltage(3.3, raw)); 
#endif
				sprintf_P(buffer, PSTR("%3d"),Probe[i].CurrentTemp);
				display.setCursor(7, i);
				display.print(buffer);
				sprintf_P(buffer, PSTR("%3d"),Probe[i].Setpoint);
				display.setCursor(15, i);
				display.print(buffer);
				// display setpoint status using special chars defined in setup()
				display.setCursor(19, i);
				if ( Probe[i].SetpointReached ) {
					display.print(char(1));
				} else {
					display.print(char(0));
				}
			} else {
				display.setCursor(3, i);
				display.print(F("No Probe         "));
			}
		}
	}
}

/*
 LOOP
*/
void loop ( void ) {
	Blynk.run();
	yield();
	timer.run();
}